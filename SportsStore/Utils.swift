//
//  Utils.swift
//  SportsStore
//
//  Created by nhatnt on 9/30/18.
//  Copyright © 2018 nhatnt. All rights reserved.
//

import Foundation

class Utils {
    class func currencyStringFromNumber(number: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter.string(from: NSNumber.init(value: number)) ?? ""
    }
}
